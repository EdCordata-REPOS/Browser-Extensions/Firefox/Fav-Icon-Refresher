function RefreshFavIcon() {


  function Start() {
    var el  = FindOrCreateFavIconElement();
    var url = el.getAttribute('href');

    DownloadFavIcon(url, function (success, params) {
      if ( success === true ) {
        SetFavIcon(el, params);
        CloseTab();
      } else {
        // if image was not downloaded update url to use google fav icon downloader and try again
        DownloadFavIcon(BuildFavIconURL(), function (success, params) {
          if ( success === true ) SetFavIcon(el, params);
          CloseTab();
        });
      }
    });
  } // Start


  function DownloadFavIcon(url, callback) {
    var xhr = new XMLHttpRequest();

    xhr.open('GET', url, true);
    xhr.responseType = 'blob';

    xhr.onload = function() {
      if (xhr.status === 200) {
        var reader = new FileReader();
        reader.onloadend = function() {
          callback(true, reader.result);
        };
        reader.readAsDataURL(xhr.response);
      } else {
        callback(false, 'Error downloading image');
      }
    };

    xhr.onerror = function() {
      callback(false, 'Network error');
    };

    xhr.send();
  } // DownloadFavIcon


  function SetFavIcon(el, image) {
    el.setAttribute('href', 'data:image/png;base64,' + image);
  } // SetFavIcon


  function FindOrCreateFavIconElement() {
    var el  = undefined;
    var els = document.getElementsByTagName('link');

    for ( var i = 0; i < els.length; i++ ) {
        if ( els[i].getAttribute('rel') == 'icon' || els[i].getAttribute('rel') == 'shortcut icon' ) {
          el = els[i];
        }
    }

    if ( el === undefined ) {
      el     = document.createElement('link');
      el.rel = 'icon';
      el.setAttribute( 'href', BuildFavIconURL() );
      document.head.appendChild(el);
    }

    return el;
  } // CreateFavIconElement


  function BuildFavIconURL() {
    return 'https://s2.googleusercontent.com/s2/favicons?domain_url=' + encodeURI(window.location.href);
  } // BuildFavIconURL


  function CloseTab() {
    setTimeout(window.close, 1000);
  } // CloseTab


  Start();
}


RefreshFavIcon();
