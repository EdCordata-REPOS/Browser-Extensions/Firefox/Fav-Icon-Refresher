let $initial, $refreshing, $log;


document.addEventListener('DOMContentLoaded', function () {
  $log        = document.querySelector('*[data-js-log]');
  $initial    = document.querySelector('*[data-js-initial]');
  $refreshing = document.querySelector('*[data-js-refreshing]');
  document.querySelector('*[data-js-start]').addEventListener('click', Start);
});


function Start() {
  let max_tabs_open               = 10;
  let bookmarks_curr_opened_count = 0;
  let bookmarks_processed         = [];
  let bookmarks                   = [];

  $initial.setAttribute('data-hidden', 'true');
  $refreshing.setAttribute('data-hidden', 'false');

  Log('Collecting bookmarks...');

  GetAllBookmarks(function (unsorted_bookmarks) {
    Log(unsorted_bookmarks.length + ' bookmarks found');

    // make sure bookmark bar items are first, so user instantly sees results
    bookmarks = (
      unsorted_bookmarks.filter(item => item.parentId === 'toolbar_____')
    ).concat(
      unsorted_bookmarks.filter(item => item.parentId !== 'toolbar_____')
    );

    Log('Loading tabs & their fav icons...');

    // get current tab
    browser.tabs.getCurrent(function(curr_tab) {
      let interval = setInterval(function() {
        if (bookmarks_processed.length <= bookmarks.length) {
          if (bookmarks_curr_opened_count < max_tabs_open) {
            bookmarks_curr_opened_count++;

            let bookmark = bookmarks.filter(item => !bookmarks_processed.includes(item.id))[0];

            bookmarks_processed.push(bookmark.id);

            if (bookmark) {
              console.log(
                'Open: ',                          bookmark.id,
                '| bookmarks_curr_opened_count: ', bookmarks_curr_opened_count,
                '| max_tabs_open: ',               max_tabs_open,
                '| bookmarks_processed: ',         bookmarks_processed.length,
                '| bookmarks: ',                   bookmarks.length,
                '| url:',                          bookmark.url
              );

              Log('Loading tabs & their fav icons... (' + bookmarks_processed.length + '/' + bookmarks.length + ')', true);

              LoadAndCloseTab(bookmark.url, curr_tab.windowId, function() {
                bookmarks_curr_opened_count--;
              });
            } else {
              bookmarks_curr_opened_count--;
            }
          }
        }

        if (bookmarks_processed.length >= bookmarks.length && bookmarks_curr_opened_count <= 0) {
          clearInterval(interval);
          Log('Done', true);
        }
      }, 50);
    });
  });
} // Start


function Log(text, clear_last) {
  if (clear_last === true) $log.children[$log.children.length - 1].remove();
  $log.innerHTML += ('<li>' + text + '</li>');
} // Log


function LoadAndCloseTab(url, window_id, callback) {
  if (url) {
    try {
      browser.tabs.create({ url: url, active: false, muted: true, windowId: window_id }).then(
        function (tab) {
          browser.tabs.executeScript(tab.id, { file: 'refresh.js' }).then(() => {
            setTimeout(callback, 3000);
          }).catch(error => {
            browser.tabs.remove(tab.id);
            callback();
          });
        },
        function(error) {
          callback();
        });
    } catch (error) {
      callback();
    }
  } else {
    callback();
  }
} // LoadAndCloseTab


function GetAllBookmarks(callback) {
  browser.bookmarks.getTree().then(async function (bookmark_tree) {
    FlattenRecursiveTree(bookmark_tree, callback);
  });
} // GetAllBookmarks


function FlattenRecursiveTree(bookmark_tree, callback) {
  let bookmarks = [];

  // flatten bookmarks recursively
  // -----------------------------
  FlattenRecursiveTreeLoop(bookmark_tree);

  function FlattenRecursiveTreeLoop(tree) {
    if (Array.isArray(tree)) {
      tree.forEach(function (bookmark) {
        FlattenRecursiveTreeLoop(bookmark, function (flattened_bookmarks) {
          bookmarks.concat(flattened_bookmarks);
        });
      });
    } else {
      bookmarks.push({ id: tree.id, url: tree.url, parentId: tree.parentId });

      if (tree.children && tree.children.length > 0) {
        FlattenRecursiveTreeLoop(tree.children, function (flattened_bookmarks) {
          bookmarks.concat(flattened_bookmarks);
        });
      }
    }
  }
  // -----------------------------

  // wait for all bookmarks to be loaded
  // -----------------------------
  let i = -1;

  let t = setInterval(function () {
    if (bookmarks.length === i) {
      clearInterval(t);
      callback(bookmarks);
    } else {
      i = bookmarks.length;
    }
  }, 500);
  // -----------------------------

} // FlattenRecursiveTree
