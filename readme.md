# Fav Icon Refresher
Simple extension to refresh bookmark fav icons in Firefox.

Get it on [Firefox Addons Page](https://addons.mozilla.org/en-US/firefox/addon/fav-icon-refresher/).


## How to use?
1. Install
2. Click on 'Fav Icon Refresher' toolbar icon
   ( <img src="src\assets\icons\main.svg" height="16"/> )
3. Click `Refresh all Fav icons` button


## License
This project is licensed under
[CC BY-SA 4.0 Deed](https://creativecommons.org/licenses/by-sa/4.0/) license.
